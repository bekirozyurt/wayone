var express = require('express');
var router = express.Router();

var http =require("http");

/* GET users listing. */
router.get('/search', function(req, res, next) {
	var q=req.query.q;
	var url="http://localhost:3001/history?search="+q;
	console.log(url);
	var serviceCallback= function(response) {
		var str = "";
		response.on('data', function (chunk) {str += chunk;});
		response.on('end', function () {
			var jsonData= JSON.parse(str);
		
			res.json(jsonData);

		});
	};
	var request=http.get(url,serviceCallback);
	request.on('error', function(e) {
		res.json(e);
	});

});

module.exports = router;
