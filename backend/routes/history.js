var express = require('express');
var router = express.Router();


 var historyPlaces = 
 [
	{
		"_id"  : 1,
		"name" : "Bursa Gezisi",
		"city" : "Bursa",
		"country" : "Türkiye",
		"description" : "Bursa Ulu Camii"
	},
	{
		"_id" : 2,
		"name" : "Hasan Keyf",
		"city" : "Adıyaman",
		"country" : "Türkiye",
		"description" : ""
	}
];

var fs=require('fs');

/* GET users listing. */
router.get('/', function(req, res, next) {
	console.log("history geldi");
	var result=[];
	var search = req.query.search;
	console.log(search);


	if (search) {
		for (var i = 0; i < historyPlaces.length; i++) {
			if(historyPlaces[i].name.toLowerCase().indexOf(search.toLowerCase())>=0 
				|| historyPlaces[i].city.toLowerCase().indexOf(search.toLowerCase())>=0
				|| historyPlaces[i].description.toLowerCase().indexOf(search.toLowerCase())>=0
				){
				result.push(historyPlaces[i]);
			}
		}
	}else{
		result = historyPlaces;
	}

	res.json(result);
});


router.get('/:id', function(req, res, next) {
	var id=req.params.id;
	var result=[];
	if (id) 
	{
		for (var i = 0; i < historyPlaces.length; i++) {
			if(historyPlaces[i]._id==id){
				result.push(historyPlaces[i]);
			}
		};
	}

	res.json(result);
});


module.exports = router;
